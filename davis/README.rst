Installs and upgrades https://github.com/tchapi/davis for small-scale
deployments.

Configuration might need to be adjusted.
Backups need to be put in place separately.

Uses SQLite as database, because
a) easier to operate,
b) easier to backup,
c) lower latency non-parallel access than, e.g. PostgreSQL.
