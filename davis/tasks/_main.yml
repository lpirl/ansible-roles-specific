- name: un/install packages
  package:
    name: "{{ item.key }}"
    state: "{{ item.value }}"
  with_dict: "{{ davis_packages | combine(davis_packages_extra) }}"


- name: sanitize root directory variable
  set_fact:
    davis_root: "{{ davis_root.rstrip('/') }}"


- name: create davis user
  user:
    name: "{{ davis_user }}"
    group: "{{ davis_group }}"


- name: create davis root
  file:
    state: directory
    dest: "{{ davis_root }}"
    owner: "{{ davis_user }}"
    group: "{{ davis_group }}"


- name: "run commands as {{ davis_user }}"
  become_user: "{{ davis_user }}"
  become: yes
  block:

    - name: fetch repository
      git:
        repo: https://github.com/tchapi/davis.git
        dest: "{{ davis_root }}"


    - name: write initial configuration
      copy:
        dest: "{{ davis_root }}/.env.local"
        mode: g+r
        force: no
        content: |
          APP_ENV=prod
          APP_SECRET={{
            lookup('password', '/dev/null',
                   chars=['ascii_lowercase', 'digits'], length=32)
          }}

          # empty to use default time zone of the server
          APP_TIMEZONE=

          DATABASE_DRIVER=sqlite
          DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.sqlite3"

          ADMIN_PASSWORD={{
            lookup('password', '/dev/null', chars='hexdigits', length=32)
          }}

          AUTH_REALM=dav

          LOG_FILE_PATH="%kernel.logs_dir%/%kernel.environment%.log"

    - name: get latest tagged version
      register: davis_tag_result
      shell: git tag -n 'v*' | tail -n 1 | cut -d' ' -f1
      args:
        chdir: "{{ davis_root }}"
      changed_when: false


    - name: checkout latest tag
      git:
        repo: https://github.com/tchapi/davis.git
        dest: "{{ davis_root }}"
        version: "{{ davis_tag_result.stdout_lines.0 }}"


    - name: install PHP dependencies
      register: composer_install_result
      command: composer install
      args:
        chdir: "{{ davis_root }}"
      changed_when: "'Nothing to install, update or remove' not in
                     composer_install_result.stderr_lines"


    - name: run database migrations
      register: davis_migrations_result
      command: >-
        bin/console
        doctrine:migrations:migrate --all-or-nothing --no-interaction
      args:
        chdir: "{{ davis_root }}"
      changed_when: "' migrations executed, 0 sql queries' not in
                     davis_migrations_result.stdout"


- name: set directory permissions
  file:
    dest: "{{ item.dest }}"
    mode: "{{ item.mode }}"
    owner: "{{ davis_user }}"
    group: "{{ davis_group }}"
    recurse: yes
  with_items:
    - dest: "{{ davis_root }}"
      mode: g+rX
    - dest: "{{ davis_root }}/var"
      mode: g+rwX
