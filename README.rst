.. image:: https://gitlab.com/lpirl/ansible-roles-specific/badges/master/pipeline.svg
  :target: https://gitlab.com/lpirl/ansible-roles-specific/pipelines
  :align: right


Ansible roles for specific applications and use cases
=====================================================

The roles in this repository configure hosts for specific applications.
This does not only include applications as in "pieces of software", but
also system configurations for specific use cases.


static files
------------

For reference (and easier browsing), all static files of all roles in
this repository (``*/files/*``) can be found `on the pages of this
project <https://lpirl.gitlab.io/ansible-roles-specific/>`__.


conventions
-----------

The `notes on conventions
<https://gitlab.com/lpirl/ansible-roles-generic/-/blob/master/conventions.rst>`__
from `gitlab.com/lpirl/ansible-roles-generic
<https://gitlab.com/lpirl/ansible-roles-generic>`__ also apply here and
might be an interesting read if you consider trying the roles in this
repository.

more specific roles (elsewhere)
-------------------------------

* `802.11p on Linux <https://gitlab.com/hpi-potsdam/osm/g5-on-linux/11p-on-linux>`__
  (for, e.g., WAVE, ITS G5)



----

History:

The roles in this repository are a rewrite of
`github.com/lpirl/ansible-roles
<https://github.com/lpirl/ansible-roles>`__
and also some scripts and configuration files previously kept in
`github.com/lpirl/admintools <https://github.com/lpirl/admintools/>`__
are now incorporated here.
