FILES_OF_ALL_ROLES_DIR ?= files-of-all-roles

TREE_HTML_HEADLINE ?= static files of \
	<a href="https://gitlab.com/lpirl/ansible-roles-specific"> \
		lpirl/ansible-roles-specific \
	</a>


$(FILES_OF_ALL_ROLES_DIR):
	mkdir -p $@
	find * -type d -name files -print0 \
	| xargs -0IX sh -c "cp -vau X/* $@"

public: $(FILES_OF_ALL_ROLES_DIR)
	rm -fr $@
	mkdir -p $@
	mv $(FILES_OF_ALL_ROLES_DIR) public
	cd $@; \
	find . -type d -print0 \
	| xargs -t0IX sh -c " \
		cd 'X'; \
		tree -aH . --noreport -T '${TREE_HTML_HEADLINE}: X' \
		--charset utf-8 . > 'index.html'"

clean:
	rm -rf \
		$(FILES_OF_ALL_ROLES_DIR) \
		public
