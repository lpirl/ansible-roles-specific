Installs ``lxd`` from snap.

Intermediate workaround until ``lxd`` is packaged for apt;
see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=768073 .
