- name: install prerequisites
  package:
    name:
      - gpg
      - ca-certificates
      - apt-transport-https

      # as of June 2020, gitlab-runner seems just recommends (not
      # requires) this dependency,which leads to a warning during
      # installation, which leads to a unusable runner which throws
      # opaque errors ("pull access denied for gitlab-runner-helper")
      - cdebootstrap


- name: add apt key
  register: add_key_result
  get_url:
    url: https://packages.gitlab.com/runner/gitlab-runner/gpgkey
    dest: /etc/apt/trusted.gpg.d/gitlab-runner.asc
    mode: a+r


- name: add apt repository
  register: add_repository_result
  blockinfile:
    dest: /etc/apt/sources.list.d/gitlab-runner.list
    create: yes
    block: >
      deb https://packages.gitlab.com/runner/gitlab-runner/{{
        ansible_distribution | lower
      }}/ {{
        ansible_distribution_release
        | regex_replace ('^oldstable$', 'bullseye')
        | regex_replace ('^stable$', 'bookworm')
        | regex_replace ('^testing$', 'trixie')
        | regex_replace ('^unstable$', 'trixie')
        | regex_replace ('^sid$', 'trixie')
      }} main


- name: pin GitLab runner service to GitLab as origin
  blockinfile:
    dest: /etc/apt/preferences.d/pin-gitlab-runner
    create: yes
    mode: "0640"
    block: |
      Explanation: prefer GitLab-provided packages
      Package: gitlab-runner
      Pin: origin packages.gitlab.com
      Pin-Priority: 1001


- name: install GitLab runner service
  register: install_gitlab_runner
  apt:
    name: gitlab-runner
    update_cache: "{{
      add_key_result.changed or add_repository_result.changed
    }}"


- name: configure global runner options
  register: configure_docker_global_options_result
  lineinfile:
    dest: /etc/gitlab-runner/config.toml
    regexp: '\s*{{ item.key | regex_escape }}\s*='
    line: '{{ item.key }} = {{ item.value }}'
    insertbefore: bof
  with_dict:
    "concurrent": "{{ gitlab_runners_concurrent }}"


- name: configure Docker executor options
  register: configure_docker_executor_options_result
  lineinfile:
    path: /etc/gitlab-runner/config.toml
    insertafter: '^\s*\[runners.docker\]'
    regexp: '^\s*{{ item.key | regex_escape }}\s*=.*'
    line: "{{ item.key }} = {{ item.value }}"
  with_dict:
    'privileged': "false"
    'memory': '"{{ gitlab_runners_memory_limit }}"'
    'cpus': '"{{ gitlab_runners_cpus }}"'


# to clean up, but also to work around
# https://gitlab.com/gitlab-org/gitlab-runner/issues/3802
- name: remove empty sections from config file
  register: remove_empty_sections_result
  replace:
    path: /etc/gitlab-runner/config.toml
    regexp: '^.*\]\s*\n((\s*\[.*)|\Z)$'
    replace: '\1'
  delay: 0
  until: not remove_empty_sections_result.changed


- name: start GitLab runner service
  service:
    name: gitlab-runner
    state: "{{
      'restarted' if
        install_gitlab_runner.changed or
        configure_docker_global_options_result.changed or
        configure_docker_executor_options_result.changed or
        remove_empty_sections_result.changed
      else 'started'
    }}"
    enabled: yes


- name: get list of existing runners
  register: get_runners_result
  shell: gitlab-runner list 2>&1 | tail -n +3 | cut -d" " -f1
  changed_when: false


- name: configure single runner instance
  include_tasks: _configure-runner.yml
  vars:
    runner_description: "{{ runner_item.key }}"
    runner_configuration: "{{
      gitlab_runner_defaults
      | combine(gitlab_runner_defaults_extra)
      | combine(runner_item.value)
    }}"
  when: runner_description not in get_runners_result.stdout_lines
  with_dict: "{{ gitlab_runners }}"
  loop_control:
    loop_var: runner_item


- name: schedule cleanup of stale Docker resources
  cron:
    job: "{{ item.value }}"
    name: >-
      clean up stale Docker {{ item.key }} (left over by GitLab runner)
    special_time: "{{ gitlab_runner_docker_prune_interval }}"
    state: "{{ 'absent' if gitlab_runner_docker_prune_interval == false
                else 'present' }}"
  with_dict:

    # Filter "until" not supported for volumes. That's why we clean up
    # everything except volumes with the "until" filter and volumes
    # separately.

    containers: >-
      docker system prune -f --filter "label!=keep"
      --filter "until=2400h" --all > /dev/null
    volumes: >-
      docker volume prune -f --filter "label!=keep" > /dev/null
