Installs, configures and registers a GitLab runner which runs jobs in
unprivileged Docker containers.

For the properties the runner is created and registered with, check
`the default variables of this role <defaults/main.yml>`__.
