Installs https://www.nomadproject.io/ from HashiCorp's APT repositories.

This role can also set up a trivial custom CA for basic security.

Unless you create all the ``nomad_*_file``s yourself (see default
variables), you'll need the ``openssl`` command on the local and remote
hosts. See also `the documentation on TLS in the Nomad docs
<https://learn.hashicorp.com/tutorials/nomad/security-enable-tls>`__.

If the remote runs systemd, a service is put in place and enabled. If
not, you must integrate Nomad in your service manager manually (sorry,
contributions welcome).
