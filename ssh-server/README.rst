A simple role to install the OpenSSH server and ensure required files
and directories exist.

(This role is mainly used to get OpenSSH server running in GitLab CI,
where things are a bit different and the system needs some modifications
before the ssh server can be started).
