This role configures a SSH chroot for per user of a group.

This is useful, e.g., to setup a SSH file server, where users need to
run a limited set of commands (rsync, sftp, …) but should not be able
to use a (proper) shell or see the parts of the file system irrelevant
to them.

By default, the group "noshell" is (created, ) used and users will get
the restricted shell "rbash" configured. See ``defaults/main.yml`` for
configuring this and other options.
