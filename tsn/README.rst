Time-sensitive Networking (TSN)
===============================

A (yet simple) role to configure hosts for Time-sensitive Networking
(TSN).

Configuration includes

* a VLAN
* IP addresses
* synchronization of hardware clocks (``ptp4l``)
* synchronization of hardware clocks to system clocks (``phc2sys``)

See `vars/main.yml <vars/main.yml>`__ to see what can be configured.

The configurations are gone after reboot.
