- name: sanity-check interface to configure for TSN
  assert:
    that:
      - tsn_physical_interface is defined
      - tsn_physical_interface in ansible_interfaces
      - tsn_physical_interface != ansible_default_ipv4.alias | default(false)
      - tsn_physical_interface != ansible_default_ipv6.alias | default(false)



- name: check if physical interface supports time stamping
  register: ethtool_result
  command: "ethtool -T {{ tsn_physical_interface }}"
  changed_when: false
  failed_when: "ethtool_result.rc != 0 or (
    'software-transmit' not in ethtool_result.stdout
    and
    'hardware-transmit' not in ethtool_result.stdout
  )"


# determine which nth IP host has on default interface, use nth IP in
# TSN subnet; e.g.: 192.168.1.42 on default interface → 43rd address
# use 10.1.1.42 for TSN
- name: derive TSN IP addresses from default interface
  register: tsn_ip_detectd
  set_fact:
    tsn_ip: "{{
      tsn_subnet
      | ipaddr(
        (
        ansible_default_ipv4.address
          | ipsubnet(
            ansible_default_ipv4.network
            ~ '/'
            ~ ansible_default_ipv4.netmask
          )
        )
        | int
        %
        (
          tsn_subnet
          | ipaddr('size')
        )
        | int
        - 1
      )
    }}"
  when: not tsn_ip is defined



- name: warn if TSN IP is not set explicitly
  debug:
    msg: >
      Variable ``tsn_ip`` not set explicitly in configuration.
      Will use {{ tsn_ip }} as derived from primary interface.
  when: tsn_ip_detectd is defined



- name: reset (remove) TSN VLAN device
  register: delete_vlan_device_result
  command: "ip link delete dev {{ tsn_vlan_interface }}"
  failed_when: delete_vlan_device_result.rc != 0 and
               'Cannot find device' not in delete_vlan_device_result.stderr
  changed_when: delete_vlan_device_result.rc != 0



- name: add TSN VLAN (with Linux packet priority (SO_PRORITY) mapped to
        VLAN priority (PCP header) 1:1)
  command: "ip link add
    link {{ tsn_physical_interface }}
    name {{ tsn_vlan_interface }}
    type vlan
    id {{ tsn_vlan }}
    egress-qos-map {% for i in range(0, 16) %}
      {{ i }}:{{ [i, 7] | min }}
    {% endfor %}
  "



- name: add IP address to (VLAN) interface
  command: "ip address add {{ tsn_ip }} brd + dev {{ tsn_vlan_interface }}"

- name: up interfaces
  command: "ip link set {{ item }} up"
  with_items:
    - "{{ tsn_physical_interface }}"
    - "{{ tsn_vlan_interface }}"
