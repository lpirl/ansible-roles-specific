- name: install prerequisites
  package:
    name:
      - gpg
      - gpg-agent
      - ca-certificates
      - apt-transport-https


- name: add apt key
  register: add_key_result
  get_url:
    url: "{{ docker_apt_repository_url }}/gpg"
    dest: /etc/apt/trusted.gpg.d/docker.asc
    mode: a+r

- name: add apt repository
  register: add_repository_result
  blockinfile:
    dest: /etc/apt/sources.list.d/docker.list
    create: yes
    block: >-
      deb
      {{ docker_apt_repository_url }}
      {{ docker_apt_repository_suite }}
      {{ docker_apt_repository_components }}


- name: pin docker-ce packages to Docker origin
  blockinfile:
    dest: /etc/apt/preferences.d/pin-docker
    create: yes
    mode: "0640"
    block: |
      Explanation: prefer Docker-provided Docker packages
      Package: containerd.io docker-ce docker-ce-*
      Pin: origin {{ docker_apt_repository_url | urlsplit('hostname') }}
      Pin-Priority: 1001


- name: install Docker packages
  register: install_docker_result
  package:
    name:
      - docker-ce
      - docker-ce-cli
    update_cache: "{{
      add_key_result.changed or add_repository_result.changed
    }}"


- name: if we are in LXC and run btrfs, check if mount option
        user_subvol_rm_allowed is present
  block:

    - name: get mountpoint of /var/lib
      register: var_lib_mount_point_result
      command: df --output=target /var/lib
      failed_when: false
      changed_when: false


    - name: check for mount option user_subvol_rm_allowed for btrfs file
            system at /var/lib
      assert:
        quiet: true
        that:
          - "{{ item.fstype != 'btrfs' or
                'user_subvol_rm_allowed' in item.options }}"
      when: item.mount == var_lib_mount_point_result.stdout_lines.1
      with_items: "{{ ansible_mounts }}"
      loop_control:
        label: "{{ item.mount }}"

  when: ansible_virtualization_type == 'lxc'


# https://github.com/docker/cli/issues/4807
- name: disable setting ulimits for Docker daemon when OS-virtualized
  replace:
    path: /etc/init.d/docker
    regexp: '[^#]ulimit'
    replace: ': #ulimit'
  when: is_os_virtualized


- name: enable and start service
  service:
    name: docker
    enabled: yes
    state: "{{
      'restarted' if  install_docker_result.changed else 'started'
    }}"
