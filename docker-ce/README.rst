Installs Docker community edition from Docker APT repositories.

This role won't touch ``/etc/docker/daemon.json`` since it is not really
possible to manage that file via Ansible while still allowing local
modifications to that file.
