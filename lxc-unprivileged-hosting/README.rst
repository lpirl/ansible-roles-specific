This role sets up unprivileged containers with plain LXC.

This roles is tested on Debian stable.

In order to manage complexity, this role has limited
flexibility/configuration options. If you find yourself needing much
more flexibility, consider using https://linuxcontainers.org/incus/ .

Developing and using this role which builds on plain LXC instead of
using, e.g., LXD is motivated by
`the concerns about running the snap-provided LXD
<https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=768073>`__ and
having deployments that solely need some mostly static, homogeneous,
and explicitly configured containers. No daemon, no database, etc.

This role will not

* configure anything inside the containers, except the ability to run
  initial commands after their creation;
* configure the network (i.e., the host's ``iptables``) for the
  containers.
